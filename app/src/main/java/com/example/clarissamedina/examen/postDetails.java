package com.example.clarissamedina.examen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by clarissamedina on 17/07/15.
 */
public class postDetails extends Activity {
    TextView titleTextView,
             authorTextView,
             publicationTextView,
             bodyTextView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.postdetails);

        titleTextView = (TextView) findViewById(R.id.titleTextView);
        authorTextView = (TextView) findViewById(R.id.authorTextView);
        bodyTextView = (TextView) findViewById(R.id.bodyTextView);



        Intent in = this.getIntent();
        String title = in.getStringExtra("title");
        String author = in.getStringExtra("Autor");
        String created = in.getStringExtra("created");
        String body = in.getStringExtra("body");

        titleTextView.setText(title);
        authorTextView.setText("Autor "+author);
        bodyTextView.setText(body);



    }

}
