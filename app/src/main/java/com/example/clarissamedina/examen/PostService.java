package com.example.clarissamedina.examen;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;

/**
 * Created by clarissamedina on 16/07/15.
 */
public interface PostService {

    @GET("/posts")
    void getPost(Callback<List<Post>> Post);

    @POST("/posts")
    void createPost(@Body Post post, Callback<Post> cb);
}
