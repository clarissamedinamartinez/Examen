package com.example.clarissamedina.examen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by clarissamedina on 18/07/15.
 */
public class newPost extends Activity {

    EditText titleEditText;
    EditText authorEditText;
    EditText bodyEditText;
    Button newPostButton;
    String title, author, body;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newpost);

        titleEditText = (EditText) findViewById(R.id.titleEditText);
        authorEditText = (EditText) findViewById(R.id.authorEditText);
        bodyEditText = (EditText) findViewById(R.id.bodyEditText);
        newPostButton = (Button) findViewById(R.id.newPostButton);


        newPostButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                title = titleEditText.getText().toString();
                author = authorEditText.getText().toString();
                body = bodyEditText.getText().toString();


                RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint("http://10.0.2.2:3000/")
                        .build();
                PostService service = restAdapter.create(PostService.class);
                service.createPost(new Post(body, author, title), new Callback<Post>() {
                    @Override
                    public void success(Post post, Response response) {
                        titleEditText.setText("");
                        authorEditText.setText("");
                        bodyEditText.setText("");

                        Intent intent = new Intent(newPost.this, MainActivity.class);
                        startActivity(intent);

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e("error",error.getMessage());
                    }
                });
            }
        });


    }

}
