package com.example.clarissamedina.examen;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import android.widget.SearchView;

import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends Activity{

    ListView postListView;
    private EditText search;
    private PostAdapter postAdapter;
    List<Post> Post;
    //SharedPreferences  settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        postListView = (ListView) findViewById(R.id.listView);
        search = (EditText) findViewById(R.id.searchEditText);

        if(isOnline()){
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("http://10.0.2.2:3000/")
                    .build();
            PostService service = restAdapter.create(PostService.class);
            postListView.setClickable(true);
            service.getPost(new Callback<List<Post>>() {
                @Override
                public void success(List<Post> posts, Response response) {

                    postAdapter = new PostAdapter(posts, getApplicationContext());
                    postListView.setAdapter(postAdapter);
                   /* SharedPreferences.Editor prefsEditor = settings.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(posts);
                    prefsEditor.putString("Objeto", json);
                    prefsEditor.commit();*/

                    search.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            postAdapter.getFilter().filter(s.toString());
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });

                }

                @Override
                public void failure(RetrofitError error) {

                }
            });

        } else {
            /*
            Gson gson = new Gson();
            SharedPreferences  settings =  getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
            String json = settings.getString("Objeto","");
            Post post = gson.fromJson(json, Post.class);
            postAdapter = new PostAdapter(post, getApplicationContext());
            postListView.setAdapter(postAdapter); */
        }


        postListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, postDetails.class);
                Post post = (Post) parent.getItemAtPosition(position);
                intent.putExtra("title", post.getTitle());
                intent.putExtra("Autor", post.getAutor());
                intent.putExtra("body", post.getBody());

                startActivity(intent);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                //openSearch();
                return true;
            case R.id.action_newPost:
                openNewPost();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }

        return false;
    }
    public void openNewPost(){
        Intent intent = new Intent(MainActivity.this, newPost.class);
        startActivity(intent);
    }

}
