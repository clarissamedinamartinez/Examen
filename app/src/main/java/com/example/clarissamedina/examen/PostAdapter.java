package com.example.clarissamedina.examen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by clarissa medina on 16/07/15.
 */
public class PostAdapter extends BaseAdapter implements Filterable {
    private List<Post> post;
    private List<Post> Originalpost;
    private ArrayList<Post> _Post;
    private Context context;


    public PostAdapter(List<Post> post, Context context){
        this.post = post;
        this.context = context;
    }

    @Override
    public int getCount() {
        return post.size();
    }

    @Override
    public Object getItem(int position) { //tipo de elementos que retorna la lista
        return post.get(position);
    }

    @Override
    public long getItemId(int position) {
        return post.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_post, null); //se carga el item post dentro del view para acceder a los elementos para cargar los datos dentro del listView

        }
        Post post = this.post.get(position);

        TextView titleTextView = (TextView) view.findViewById(R.id.titleTextView);
        TextView authorTextView = (TextView) view.findViewById(R.id.authorTextView);
        TextView contentTextView = (TextView) view.findViewById(R.id.contentTextView);
        titleTextView.setText(post.getTitle());

        authorTextView.setText("Autor "+post.getAutor());

        //validacion de 70 caracteres

        String Content = post.getBody();
        if(Content.length()<= 70){
            contentTextView.setText(Content);
        }else {
            contentTextView.setText(Content.substring(0,70) + "...");
        }


        return view;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint,FilterResults results) {

                post = (List<Post>) results.values; // has the filtered values
                notifyDataSetChanged();  // notifies the data with new filtered values
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                List<Post> FilteredArrList = new ArrayList<Post>();

                if (Originalpost == null) {
                    Originalpost = new ArrayList<Post>(post); // saves the original data in mOriginalValues
                }

                if (constraint == null || constraint.length() == 0) {

                    // set the Original result to return
                    results.count = Originalpost.size();
                    results.values = Originalpost;

                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < Originalpost.size(); i++) {
                        String titulo = Originalpost.get(i).getTitle();
                        String autor = Originalpost.get(i).getAutor();
                        if (titulo.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(Originalpost.get(i));
                        } else if (autor.toLowerCase().startsWith(constraint.toString())) {
                            FilteredArrList.add(Originalpost.get(i));
                        }

                    }
                    // set the Filtered result to return
                    results.count = FilteredArrList.size();
                    results.values = FilteredArrList;
                }
                return results;
            }
        };
        return filter;

    }
}
