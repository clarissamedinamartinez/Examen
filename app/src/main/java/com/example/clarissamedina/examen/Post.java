package com.example.clarissamedina.examen;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by clarissamedina on 16/07/15.
 */
public class Post {
    private int id=1;
    private String title;
    private String author;
    private String created;
    private String body;

    public Post(String body, String author, String title) {
        this.id ++;
        this.body = body;
        this.author = author;
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getAutor() {
        return author;
    }

    public void setAutor(String author) {
        author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("title", this.title);
            jsonObject.put("body", this.body);
            jsonObject.put("author",this.author);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }


}
